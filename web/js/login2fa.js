$(function(){
    $('#btn-1').click(function(){
        // debugger;
        var username = $('#loginform-username').val();
        var pass = $('#loginform-password').val();

        $.ajax({
            url: "validate-user",
            type: 'POST',
            data: {
                username: username,
                password: pass
            },
            success: function(data){
                if (data != 'ok'){
                    alert(data);
                } else {
                    $('#step-1').addClass('hidden');
                    $('#btn-1').addClass('hidden');
                    $('#step-2').removeClass('hidden');
                    $('#btn-2').removeClass('hidden');
                }
            }
        })
    });

    $('#btn-2').click(function(){
        var code = $('#loginform-code').val();
        var username = $('#loginform-username').val();
        var pass = $('#loginform-password').val();
        $.ajax({
            url: "validate-code",
            type: 'POST',
            data: {
                code: code,
                username: username,
                password: pass
            },
            success: function (data) {
                alert(data);
            }
        })
    });
});