<?php

namespace app\modules\api\controllers;

use app\models\AccountingReport;
use app\models\Bots;
use app\models\BotsAnswers;
use app\models\BotsDialogs;
use app\models\Companies;
use app\models\CompanySettings;
use app\models\DailyReport;
use app\models\Dispatch;
use app\models\DispatchChats;
use app\models\DispatchConnect;
use app\models\DispatchRegist;
use app\models\DispatchStatus;
use app\models\Logs;
use app\models\Message;
use app\models\Proxy;
use app\models\Settings;
use app\models\Users;
use yii\helpers\Json;
use yii\rest\ActiveController;
//use yii\helpers\ArrayHelper;
use yii\web\Response;
use Yii;
//use yii\data\ActiveDataProvider;
//use yii\web\NotFoundHttpException;
//use yii\web\Controller;


//use app\models\Clients;

/**
 * Default controller for the `api` module
 */
class VkController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['call-black'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    private static $token = '879383aeeea5424b71fc4df83471bfd56751f93dd69a502f3ae4fee28bef3ec6851460f5a86dfa9dc7772';
    private static $v = '5.0';
    private $button_finish =  [["animals" => '21'], "21", "red"]; //Код кнопки 'Fish'
    private $button_photo =  [["command" => 'Картинка'], "Картинка", "red"]; // Код кнопки '<< Назад'
    private $button_button= [["animals" => 'Кнопки'], "Кнопки", "white"]; // Код кнопки 'Горбуша'
    private $button_goldfish = [["animals" => 'Goldfish'], "Золотая рыбка", "blue"]; // Код кнопки 'Золотая рыбка'
    private $button_plotva = [["animals" => 'Plotva'], "Плотва", "green"]; // Код кнопки 'Плотва'


    public function actionCallBlack()
    {


        if (!isset($_REQUEST)) {
            return;
        }
        $tokenGroop = '879383aeeea5424b71fc4df83471bfd56751f93dd69a502f3ae4fee28bef3ec6851460f5a86dfa9dc7772';
        //$data = json_decode(file_get_contents('php://input'));

        //$userVk = '252341158,13390257,333043290';

        $data = json_decode(file_get_contents('php://input'));


        $this->sendOK();

        //Проверяем, что находится в поле "type"
        switch ($data->type) {

            //Если это уведомление о новом сообщении...
            case 'message_new':


                //...получаем id его автора
                $user_id = $data->object->user_id;
                $text = $data->object->body;
                $user_info = json_decode(file_get_contents("https://api.vk.com/method/users.get?user_ids={$user_id}&access_token={$tokenGroop}&v=5.0"));
                $user_name = $user_info->response[0]->first_name;


                switch ($data->object->body) {
                    case '21':

                        $message = "Hello, {$user_name}! получили 21";
                        $this->sendButton($user_id, $message, [ //Отправляем кнопки пользователю
                            [$this->button_button]
                        ]);

                        break;

                    case 'Начать':

                        break;


                    case 'Кнопки':

                        $this->sendButton($user_id, 'Вот такие, выбирай', [ //Отправляем кнопки пользователю
                            [$this->button_finish,$this->button_photo,$this->button_finish],
                            [$this->button_finish]
                        ]);

                        break;

                    case 'Картинка':

                        $this->sendImage($user_id, 'https://cdn.dailyjigsawpuzzles.net/images/original_overlayed/6b26cb53e97a0b3b0cd7d988ad029a3d.jpg');

                        break;
                }

                break;

        }

    }

    /**
     * Отправить сообщение пользователю
     * @param int $sendID Идентификатор получателя
     * @param string $message Сообщение
     * @return mixed|null
     */
    public function sendDocMessage($sendID, $id_owner, $id_doc){
        if ($sendID != 0 and $sendID != '0') {
            return $this->request('messages.send',array('attachment'=>"doc". $id_owner . "_" . $id_doc,'user_id'=>$sendID));
        } else {
            return true;
        }
    }

    function sendMessage($sendID,$message){
        if ($sendID != 0 and $sendID != '0') {
            return $this->request('messages.send',array('message'=>$message, 'user_id'=>$sendID));
        } else {
            return true;
        }
    }

    function sendOK(){
        ini_set('display_errors','Off');
        echo 'ok';
        $response_length = ob_get_length();
        // check if fastcgi_finish_request is callable
        if (is_callable('fastcgi_finish_request')) {
            /*
             * This works in Nginx but the next approach not
             */
            session_write_close();
            fastcgi_finish_request();

            return;
        }

        ignore_user_abort(true);

        ob_start();
        $serverProtocole = filter_input(INPUT_SERVER, 'SERVER_PROTOCOL', FILTER_SANITIZE_STRING);
        header($serverProtocole.' 200 OK');
        header('Content-Encoding: none');
        header('Content-Length: '. $response_length);
        header('Connection: close');

        ob_end_flush();
        ob_flush();
        flush();
    }

    function sendButton($sendID, $message, $gl_massiv = [], $one_time = False) {
        $buttons = [];
        $i = 0;
        foreach ($gl_massiv as $button_str) {
            $j = 0;
            foreach ($button_str as $button) {
                $color = $this->replaceColor($button[2]);
                $buttons[$i][$j]["action"]["type"] = "text";
                if ($button[0] != null)
                    $buttons[$i][$j]["action"]["payload"] = json_encode($button[0], JSON_UNESCAPED_UNICODE);
                $buttons[$i][$j]["action"]["label"] = $button[1];
                $buttons[$i][$j]["color"] = $color;
                $j++;
            }
            $i++;
        }
        $buttons = array(
            "one_time" => $one_time,
            "buttons" => $buttons);
        $buttons = json_encode($buttons, JSON_UNESCAPED_UNICODE);
        //echo $buttons;
        return $this->request('messages.send',array('message'=>$message, 'user_id'=>$sendID, 'keyboard'=>$buttons));
    }

    function sendDocuments($sendID, $selector = 'doc'){
        if ($selector == 'doc')
            return $this->request('docs.getMessagesUploadServer',array('type'=>'doc','user_id'=>$sendID));
        else
            return $this->request('photos.getMessagesUploadServer',array('user_id'=>$sendID));
    }

    function saveDocuments($file, $titile){
        return $this->request('docs.save',array('file'=>$file, 'title'=>$titile));
    }

    function savePhoto($photo, $server, $hash){
        return $this->request('photos.saveMessagesPhoto',array('photo'=>$photo, 'server'=>$server, 'hash' => $hash));
    }

    /**
     * Запрос к VK
     * @param string $method Метод
     * @param array $params Параметры
     * @return mixed|null
     */
    function request($method,$params=array()){
        $url = 'https://api.vk.com/method/'.$method;
        $params['access_token']=self::$token;
        $params['v']=self::$v;
        if (function_exists('curl_init')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type:multipart/form-data"
            ));
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            $result = json_decode(curl_exec($ch), True);
            curl_close($ch);
        } else {
            $result = json_decode(file_get_contents($url, true, stream_context_create(array(
                'http' => array(
                    'method'  => 'POST',
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'content' => http_build_query($params)
                )
            ))), true);
        }
        if (isset($result['response']))
            return $result['response'];
        else
            return $result;
    }

    private function replaceColor($color) {
        switch ($color) {
            case 'red':
                $color = 'negative';
                break;
            case 'green':
                $color = 'positive';
                break;
            case 'white':
                $color = 'default';
                break;
            case 'blue':
                $color = 'primary';
                break;

            default:
                # code...
                break;
        }
        return $color;
    }

    private function sendFiles($url, $local_file_path, $type = 'file') {
        $post_fields = array(
            $type => new CURLFile(realpath($local_file_path))
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type:multipart/form-data"
        ));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
        $output = curl_exec($ch);
        return $output;
    }

    function sendImage($id, $local_file_path)
    {
        $upload_url = $this->sendDocuments($id, 'photo')['upload_url'];

        $answer_vk = json_decode($this->sendFiles($upload_url, $local_file_path, 'photo'), true);

        $upload_file = $this->savePhoto($answer_vk['photo'], $answer_vk['server'], $answer_vk['hash']);

        $this->request('messages.send', array('attachment' => "photo" . $upload_file[0]['owner_id'] . "_" . $upload_file[0]['id'], 'user_id' => $id));

        return 1;
    }

}
