<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Payment */
?>
<div class="payment-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'create_datetime',
            'driver_id',
            'amount',
            'status',
            'owner_payment_id',
            'payment_datetime',
            'qiwi_number',
            'qiwi_answer',
        ],
    ]) ?>

</div>
