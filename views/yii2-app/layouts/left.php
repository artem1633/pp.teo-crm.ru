<?php

use app\models\Users;
use yii\helpers\Html;

$permission = Yii::$app->user->identity->permission;
$id = Yii::$app->user->identity->id;
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <?php

            $items = [
                ['label' => 'Menu', 'options' => ['class' => 'header']],
                ['label' => 'Пользователи', 'icon' => 'users', 'url' => ['/users'],],
                [
                    'label' => 'Справочники',
                    'icon' => 'book',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Настройки', 'icon' => 'file-text-o', 'url' => ['/settings'],],
                    ],
                ],
                ['label' => 'Профиль', 'icon' => 'file-text-o', 'url' => ['users/update', 'id' => Yii::$app->user->identity->id]],

            ];

        if(isset(Yii::$app->user->identity->id))
            {
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                        'items' => $items,
                    ]
                );
            }
        ?>

    </section>

</aside>
