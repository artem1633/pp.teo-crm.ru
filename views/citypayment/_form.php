<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CityPayment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="city-payment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'payment_datetime')->textInput() ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'owner_receipt')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'remains')->textInput() ?>

    <?= $form->field($model, 'percent_retention')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
