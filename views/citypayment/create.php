<?php

/* @var $this yii\web\View */
/* @var $model app\models\CityPayment */

?>
<div class="city-payment-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
