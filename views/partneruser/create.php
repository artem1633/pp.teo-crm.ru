<?php

/* @var $this yii\web\View */
/* @var $model app\models\PartnerUser */

?>
<div class="partner-user-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
