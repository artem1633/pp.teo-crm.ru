<?php

/* @var $this yii\web\View */
/* @var $model app\models\PartnerUser */
?>
<div class="partner-user-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
