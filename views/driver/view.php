<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Driver */
?>
<div class="driver-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'surname',
            'name',
            'patronymic',
            'call_sign',
            'qiwi_number',
            'outer_id',
        ],
    ]) ?>

</div>
