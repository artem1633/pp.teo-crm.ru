<?php

/* @var $this yii\web\View */
/* @var $model app\models\Partner */
?>
<div class="partner-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
