<?php

/* @var $this yii\web\View */
/* @var $model app\models\Partner */

?>
<div class="partner-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
