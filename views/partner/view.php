<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Partner */
?>
<div class="partner-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'outer_city_id',
            'api_key',
            'percent',
            'time_to_payment:datetime',
            'month_limit',
            'day_limit',
            'transaction_limit',
            'max_payment',
            'access_to_payments',
            'amount_percent',
            'balance',
            'waiting_recharge',
        ],
    ]) ?>

</div>
