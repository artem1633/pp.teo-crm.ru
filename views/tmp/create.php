<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tmp */

?>
<div class="tmp-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
