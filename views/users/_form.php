<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">


        <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-md-8">
                <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'balance')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'phone')->widget(MaskedInput::className(), [
                                    'mask' => '+7 (999) 999 99 99',
                                ])?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'login')->textInput([
                    'maxlength' => true,
                    'type' => 'text',
                ]) ?>
            </div>
            <div class="col-md-4">
                <?= $model->isNewRecord ? $form->field($model, 'password')->textInput(['maxlength' => true]) : $form->field($model, 'new_password')->textInput(['maxlength' => true]) ?>
            </div>
        </div>


        <?= $form->field($model, 'qiwi')->textInput(['maxlength' => true]) ?>

        <?php if (!Yii::$app->request->isAjax){ ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>


    
</div>
