<?php

namespace app\models;

/**
 * This is the model class for table "city_payment".
 *
 * @property int $id
 * @property string $payment_datetime Дата и время
 * @property int $amount Сумма
 * @property string $status Статус - Резерв/Зачислено
 * @property string $owner_receipt Плательщик
 * @property int $remains Остаток на момент зачисления
 * @property double $percent_retention Процент списания
 */
class CityPayment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'city_payment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['payment_datetime'], 'safe'],
            [['amount', 'remains'], 'integer'],
            [['percent_retention'], 'number'],
            [['status', 'owner_receipt'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_datetime' => 'Дата и время',
            'amount' => 'Сумма',
            'status' => 'Статус - Резерв/Зачислено',
            'owner_receipt' => 'Плательщик',
            'remains' => 'Остаток на момент зачисления',
            'percent_retention' => 'Процент списания',
        ];
    }
}
