<?php

namespace app\models;

/**
 * This is the model class for table "partner_user".
 *
 * @property int $id
 * @property string $surname Фамилия
 * @property string $name Имя
 * @property string $patronymic Отчество
 * @property int $phone Телефон. Напр. 79999999999
 * @property int $accept_offer Согласие с офертой
 * @property string $link_offer Ссылка на оферту
 * @property string $text_offer Текст оферты
 */
class PartnerUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partner_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['phone', 'accept_offer'], 'integer'],
            [['link_offer', 'text_offer'], 'string'],
            [['surname', 'name', 'patronymic'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'surname' => 'Фамилия',
            'name' => 'Имя',
            'patronymic' => 'Отчество',
            'phone' => 'Телефон. Напр. 79999999999',
            'accept_offer' => 'Согласие с офертой',
            'link_offer' => 'Ссылка на оферту',
            'text_offer' => 'Текст оферты',
        ];
    }
}
