<?php

namespace app\models;

/**
 * This is the model class for table "payment".
 *
 * @property int $id
 * @property string $create_datetime Дата и время создания выплаты
 * @property string $driver_id Код водителя
 * @property int $amount Сумма выплаты
 * @property string $status Статус
 * @property int $owner_payment_id Создатель выплаты
 * @property string $payment_datetime Дата и время выплаты
 * @property int $qiwi_number Номер QIWI
 * @property string $qiwi_answer Ответ от киви
 */
class Payment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['create_datetime', 'payment_datetime'], 'safe'],
            [['amount', 'owner_payment_id', 'qiwi_number'], 'integer'],
            [['driver_id', 'status', 'qiwi_answer'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_datetime' => 'Дата и время создания выплаты',
            'driver_id' => 'Код водителя',
            'amount' => 'Сумма выплаты',
            'status' => 'Статус',
            'owner_payment_id' => 'Создатель выплаты',
            'payment_datetime' => 'Дата и время выплаты',
            'qiwi_number' => 'Номер QIWI',
            'qiwi_answer' => 'Ответ от киви',
        ];
    }
}
