<?php

namespace app\models;

/**
 * This is the model class for table "driver".
 *
 * @property int $id
 * @property string $surname Фамилия
 * @property string $name Имя
 * @property string $patronymic Отчество
 * @property string $call_sign Позывной
 * @property int $qiwi_number номер QIWI кошелька
 * @property string $outer_id Внешний ID
 */
class Driver extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'driver';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['qiwi_number'], 'integer'],
            [['surname', 'name', 'patronymic', 'call_sign', 'outer_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'surname' => 'Фамилия',
            'name' => 'Имя',
            'patronymic' => 'Отчество',
            'call_sign' => 'Позывной',
            'qiwi_number' => 'номер QIWI кошелька',
            'outer_id' => 'Внешний ID',
        ];
    }
}
