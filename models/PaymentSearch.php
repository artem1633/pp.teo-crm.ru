<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PaymentSearch represents the model behind the search form about `app\models\Payment`.
 */
class PaymentSearch extends Payment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'amount', 'owner_payment_id', 'qiwi_number'], 'integer'],
            [['create_datetime', 'driver_id', 'status', 'payment_datetime', 'qiwi_answer'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Payment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'create_datetime' => $this->create_datetime,
            'amount' => $this->amount,
            'owner_payment_id' => $this->owner_payment_id,
            'payment_datetime' => $this->payment_datetime,
            'qiwi_number' => $this->qiwi_number,
        ]);

        $query->andFilterWhere(['like', 'driver_id', $this->driver_id])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'qiwi_answer', $this->qiwi_answer]);

        return $dataProvider;
    }
}
