<?php

namespace app\models;

/**
 * This is the model class for table "partner".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $outer_city_id ID из сервиса CityMobil
 * @property string $api_key АПИ ключ от CityMobil
 * @property double $percent Процент ...
 * @property int $time_to_payment Время до зачисления (в часах) ...
 * @property int $month_limit Месячный лимит суммы водителя ...
 * @property int $day_limit Суточный лимит суммы водителя ...
 * @property int $transaction_limit Лимит операции водителя ...
 * @property int $max_payment Максимальная сумма платежа ...
 * @property int $access_to_payments Доступ к выплатам (да/нет)
 * @property double $amount_percent Процент от суммы ...
 * @property int $balance Баланс
 * @property int $waiting_recharge Баланс ожидает поступления ...
 */
class Partner extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partner';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['percent', 'amount_percent'], 'number'],
            [['time_to_payment', 'month_limit', 'day_limit', 'transaction_limit', 'max_payment', 'access_to_payments', 'balance', 'waiting_recharge'], 'integer'],
            [['name', 'outer_city_id', 'api_key'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'outer_city_id' => 'ID из сервиса CityMobil',
            'api_key' => 'АПИ ключ от CityMobil',
            'percent' => 'Процент ...',
            'time_to_payment' => 'Время до зачисления (в часах) ...',
            'month_limit' => 'Месячный лимит суммы водителя ...',
            'day_limit' => 'Суточный лимит суммы водителя ...',
            'transaction_limit' => 'Лимит операции водителя ...',
            'max_payment' => 'Максимальная сумма платежа ...',
            'access_to_payments' => 'Доступ к выплатам (да/нет)',
            'amount_percent' => 'Процент от суммы ...',
            'balance' => 'Баланс',
            'waiting_recharge' => 'Баланс ожидает поступления ...',
        ];
    }
}
