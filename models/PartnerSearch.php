<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PartnerSearch represents the model behind the search form about `app\models\Partner`.
 */
class PartnerSearch extends Partner
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'time_to_payment', 'month_limit', 'day_limit', 'transaction_limit', 'max_payment', 'access_to_payments', 'balance', 'waiting_recharge'], 'integer'],
            [['name', 'outer_city_id', 'api_key'], 'safe'],
            [['percent', 'amount_percent'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Partner::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'percent' => $this->percent,
            'time_to_payment' => $this->time_to_payment,
            'month_limit' => $this->month_limit,
            'day_limit' => $this->day_limit,
            'transaction_limit' => $this->transaction_limit,
            'max_payment' => $this->max_payment,
            'access_to_payments' => $this->access_to_payments,
            'amount_percent' => $this->amount_percent,
            'balance' => $this->balance,
            'waiting_recharge' => $this->waiting_recharge,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'outer_city_id', $this->outer_city_id])
            ->andFilterWhere(['like', 'api_key', $this->api_key]);

        return $dataProvider;
    }
}
