<?php

use yii\db\Migration;

/**
 * Class m181001_191102_change_users_table
 */
class m181001_191102_change_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('users', 'fio', 'surname');
        $this->addCommentOnColumn('users', 'surname', 'Фамилия');
        $this->addColumn('users', 'name', $this->string()->comment('Имя'));
        $this->addColumn('users', 'middle_name', $this->string()->comment('Отчество'));
        $this->addColumn('users', 'phone', $this->string()->comment('Телефон'));
        $this->addColumn('users', 'qiwi', $this->string()->comment('Номер QIWI'));
        $this->addColumn('users', 'payment_account', $this->integer(20)->comment('Расчетный счет'));
        $this->addColumn('users', 'bik_bank', $this->integer(9)->comment('Бик Банка'));
        $this->addColumn('users', 'recived_money', $this->integer()->comment('Куда получать деньги'));
        $this->addColumn('users', 'telegram_nickname', $this->string()->comment('Ник в телеграме'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181001_191102_change_users_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181001_191102_change_users_table cannot be reverted.\n";

        return false;
    }
    */
}
