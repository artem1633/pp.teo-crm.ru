<?php

use yii\db\Migration;

/**
 * Handles the creation of table `driver`.
 */
class m181106_185527_create_driver_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('driver', [
            'id' => $this->primaryKey(),
            'surname' => $this->string()->comment('Фамилия'),
            'name' => $this->string()->comment('Имя'),
            'patronymic' => $this->string()->comment('Отчество'),
            'call_sign' => $this->string()->comment('Позывной'),
            'qiwi_number' => $this->integer()->comment('номер QIWI кошелька'),
            'outer_id' => $this->string()->comment('Внешний ID'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('driver');
    }
}
