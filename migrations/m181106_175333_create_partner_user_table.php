<?php

use yii\db\Migration;

/**
 * Handles the creation of table `partner_user`.
 */
class m181106_175333_create_partner_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('partner_user', [
            'id' => $this->primaryKey(),
            'surname' => $this->string()->comment('Фамилия'),
            'name' => $this->string()->comment('Имя'),
            'patronymic' => $this->string()->comment('Отчество'),
            'phone' => $this->integer(11)->comment('Телефон. Напр. 79999999999'),
            'accept_offer' => $this->smallInteger(1)->comment('Согласие с офертой'),
            'link_offer' => $this->text()->comment('Ссылка на оферту'),
            'text_offer' => $this->text()->comment('Текст оферты'),
        ]);
        $this->addCommentOnTable('partner_user', 'Водители партнера');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('partner_user');
    }
}
