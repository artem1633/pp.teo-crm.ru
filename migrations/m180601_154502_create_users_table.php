<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m180601_154502_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'fio' => $this->string(255)->comment('ФИО'),
            'login' => $this->string(255)->unique()->comment('Логин'),
            'password' => $this->string(255)->comment('Пароль'),
            'permission' => $this->string(255)->comment('Должность'), //administrator-Админ; agent-Агент; activator-Активатор
            'balance' => $this->float()->defaultValue(0)->comment('Баланс'),
            'party_system_id' => $this->integer()->comment('Ид сторонней системы'),
        ]);

        $this->addCommentOnTable('users', 'Пользователи');

        $this->insert('users',array(
            'fio' => 'Иванов Иван Иванович',
            'login' => 'admin',
            'password' => md5('admin'),
            'permission' => 'administrator',
            'balance' => 0,
            'party_system_id' => null,
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
