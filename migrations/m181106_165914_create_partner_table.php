<?php

use yii\db\Migration;

/**
 * Handles the creation of table `partner`.
 */
class m181106_165914_create_partner_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('partner', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'outer_city_id' => $this->string()->comment('ID из сервиса CityMobil'),
            'api_key' => $this->string()->comment('АПИ ключ от CityMobil'),
            'percent' => $this->double()->comment('Процент ...'),
            'time_to_payment' => $this->integer()->comment('Время до зачисления (в часах) ...'),
            'month_limit' => $this->integer()->comment('Месячный лимит суммы водителя ...'),
            'day_limit' => $this->integer()->comment('Суточный лимит суммы водителя ...'),
            'transaction_limit' => $this->integer()->comment('Лимит операции водителя ...'),
            'max_payment' => $this->integer()->comment('Максимальная сумма платежа ...'),
            'access_to_payments' => $this->smallInteger(1)->comment('Доступ к выплатам (да/нет)'),
            'amount_percent' => $this->double()->comment('Процент от суммы ...'),
            'balance' => $this->integer()->comment('Баланс'),
            'waiting_recharge' => $this->integer()->comment('Баланс ожидает поступления ...')
        ]);
        $this->addCommentOnTable('partner', 'Партнеры');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('partner');
    }
}
