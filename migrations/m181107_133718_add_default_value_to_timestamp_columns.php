<?php

use yii\db\Migration;

/**
 * Class m181107_133718_add_default_value_to_timestamp_columns
 */
class m181107_133718_add_default_value_to_timestamp_columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->alterColumn('city_payment', 'payment_datetime', $this->timestamp()->defaultValue(date('Y-m-d H:i:s'), time()));
        $this->alterColumn('payment', 'create_datetime', $this->timestamp()->defaultValue(date('Y-m-d H:i:s'), time()));
        $this->alterColumn('payment', 'payment_datetime', $this->timestamp()->defaultValue(date('Y-m-d H:i:s'), time()));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181107_133718_add_default_value_to_timestamp_columns cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181107_133718_add_default_value_to_timestamp_columns cannot be reverted.\n";

        return false;
    }
    */
}
