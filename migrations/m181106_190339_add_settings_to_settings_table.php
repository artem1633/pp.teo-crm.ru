<?php

use yii\db\Migration;

/**
 * Class m181106_190339_add_settings_to_settings_table
 */
class m181106_190339_add_settings_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->delete('settings', ['key' => 'amount_for_one_activation']);
        $this->delete('settings', ['key' => 'amount_in_one_minute']);

        $this->alterColumn('settings', 'value', $this->text());

        $this->insert('settings', [
            'name' => 'Текст оферты',
            'key' => 'offer',
            'value' => '',
        ]);

        $this->insert('settings', [
            'name' => 'АПИ ключ бота телеграм',
            'key' => 'telegram_api_bot_key',
            'value' => '',
        ]);

        $this->insert('settings', [
            'name' => 'Наименование телеграм группы',
            'key' => 'telegram_group_name',
            'value' => '',
        ]);

        $this->insert('settings', [
            'name' => 'Время отправки отчета',
            'key' => 'report_time',
            'value' => '',
        ]);

        $this->insert('settings', [
            'name' => 'АПИ ключ CityMobil',
            'key' => 'api_key_city_mobile',
            'value' => '',
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181106_190339_add_settings_to_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181106_190339_add_settings_to_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
