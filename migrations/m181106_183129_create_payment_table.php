<?php

use yii\db\Migration;

/**
 * Handles the creation of table `payment`.
 */
class m181106_183129_create_payment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('payment', [
            'id' => $this->primaryKey(),
            'create_datetime' => $this->timestamp()->comment('Дата и время создания выплаты')->defaultValue(date('Y-m-d H:i:s'), time()),
            'driver_id' =>$this->string()->comment('Код водителя'),
            'amount' => $this->integer()->comment('Сумма выплаты'),
            'status' => $this->string()->comment('Статус'),
            'owner_payment_id' => $this->integer()->comment('Создатель выплаты'),
            'payment_datetime' => $this->timestamp()->comment('Дата и время выплаты')->defaultValue(date('Y-m-d H:i:s'), time()),
            'qiwi_number' => $this->integer()->comment('Номер QIWI'),
            'qiwi_answer' => $this->string()->comment('Ответ от киви')

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('payment');
    }
}
