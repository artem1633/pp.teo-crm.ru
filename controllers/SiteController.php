<?php

namespace app\controllers;

use app\models\Settings;
use app\models\Tmp;
use app\models\Users;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get', 'post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->render('index');
        } else {
            return $this->redirect(['site/login']);
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */

    public function actionAvtorizatsiya()
    {
        if (isset(Yii::$app->user->identity->id)) {
            return $this->render('error');
        } else {
            Yii::$app->user->logout();
            $this->redirect(['login']);
        }
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return void
     * @throws \app\models\NotFoundHttpException
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        $this->redirect(['login']);
    }

    public function actionInstruksiya()
    {
        return $this->render('instruksiya');
    }

    public function actionMenuPosition()
    {
        $session = Yii::$app->session;
        if ($session['menu'] == null | $session['menu'] == 'large') $session['menu'] = 'small';
        else $session['menu'] = 'large';

        return $session['menu'];
    }

    /**
     * @return bool|string|Response
     */
    public function actionValidateUser()
    {
        $request = Yii::$app->request;

        $username = $request->post('username');
        $password = $request->post('password');

        $user = Users::find()->where(['login' => $username])->one();

        if (!$user || $user->password != md5($password)) {
            return 'Неверный логин или пароль';
        }

        //Если нет настроек СМС шлюза или номера телефона - входим без смс авторизации
        if (!$user->phone || !Settings::getSettingValueByKey('sms_gate_login') ||  !Settings::getSettingValueByKey('sms_gate_password')){
            $model = new LoginForm();
            $model->username = $username;
            $model->password = $password;
            $model->login();
            return $this->goBack();
        }

        //Формируем данные для записи во временную таблицу
        $tmp_model = new Tmp();

        $tmp_model->code = rand(10000, 99999);
        $tmp_model->user_id = $user->id;
        $tmp_model->code_create_at = date(DATE_ATOM, time());
        $tmp_model->num_wrong_code = 0;
        $tmp_model->save();

        //Отправляем смс код авторизации

        $phone = preg_replace('/[^0-9]/', '', $user->phone);

        $phone = mb_substr($phone, strlen($phone) - 10);
//        $result = 'Accepted;';
        $result = $this->requestSMS('+7' . $phone, ' Код авторизации "ВП": ' . $tmp_model->code);

        if (mb_strpos($result, 'cepted;')) {
            return 'ok';
        } else {
            return Json::encode($result);
        }
    }

    /**
     * @param $phone_number
     * @param $message
     * @return bool|string
     */
    protected function requestSMS($phone_number, $message)
    {
        $url = 'http://api.iqsms.ru/messages/v2/send/';

        $params['login'] = Settings::getSettingValueByKey('sms_gate_login');
        $params['password'] = Settings::getSettingValueByKey('sms_gate_password');
        $params['phone'] = $phone_number;
        $params['text'] = $message;
        $params['flash'] = 1;

        $result = file_get_contents($url, false, stream_context_create([
            'http' => [
                'request_fulluri' => true,
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            ]
        ]));

        return $result;

    }

    public function actionValidateCode(){

        $request = Yii::$app->request;
        $code = $request->post('code');
        $username = $request->post('username');
        $password = $request->post('password');

        Yii::warning($code . ' ' . $username . ' ' . $password, __METHOD__);


        $user = Users::find()->where(['login' => $username])->one();

        Yii::warning($user->id, __METHOD__);

        $tmp_model = Tmp::find()->where(['user_id' => $user->id])->one();

        if ($tmp_model->code == $code){
            $model = new LoginForm();
            $model->username = $username;
            $model->password = $password;
            $model->login();
            $tmp_model = Tmp::deleteAll('user_id=:user_id',[':user_id' => $user->id]);
            return $this->goBack();
        } else {
            $tmp_model->save();
            return 'Неверный СМС код подтверждения';
        }
    }




}
